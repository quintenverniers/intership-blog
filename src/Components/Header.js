import React from 'react';

import { Link } from "react-router-dom";
import './Header.css';

function Header() {
  return (
    <div className="header__container">
        <div className="header__wrapper">
            <Link to="/home" href="#default" className="logo">Q</Link>
        </div>
    </div>
  );
}

export default Header;
