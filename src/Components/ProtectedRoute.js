import React from "react";
import {
  Route,
  Redirect,
} from "react-router-dom";
import {fire} from '../flame/fire';

function ProtectedRoute({ children, ...rest }) {
    return (
      <Route
        {...rest}
        render={({ location }) =>
          fire.auth().currentUser ? (
            children
          ) : (
            <Redirect
              to={{
                pathname: "/",
                state: { from: location }
              }}
            />
          )
        }
      />
    );
  }

  export default ProtectedRoute;