import React from 'react';
import {fire} from '../flame/fire';
import { useHistory } from "react-router-dom";
import './Footer.css';

function Footer() {
    let history = useHistory();
    function LogOut() {
        fire.auth().signOut().then(function() {
            history.push("/");
          }, function(error) {
            // An error happened.
          });
    }

  return (
    <div className="footer__container">
        <p onClick={LogOut}>Logout</p>
    </div>
  );
}

export default Footer;
