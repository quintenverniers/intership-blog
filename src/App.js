import React from 'react';
import Home from './Pages/Home';
import Post from './Pages/Post';
import Login from './Pages/Login';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import ProtectedRoute from './Components/ProtectedRoute';

function App() {
  return (
      <Router>
        <Switch>
          <Route exact path="/">
            <Login />
          </Route>
          <ProtectedRoute path="/home">
            <Home />
          </ProtectedRoute>
          <ProtectedRoute path="/post/:id">
            <Post />
          </ProtectedRoute>
        </Switch>
    </Router>
  );
}

export default App;
