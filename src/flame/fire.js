import * as firebase from "firebase/app";
import "firebase/auth";
import "firebase/firestore";

const config = {
  apiKey: "AIzaSyDwrUEoi7GR63lR_uZ4hhyJf5kahS_hv2g",
  authDomain: "quintenverniers-blog.firebaseapp.com",
  databaseURL: "https://quintenverniers-blog.firebaseio.com",
  projectId: "quintenverniers-blog",
  storageBucket: "quintenverniers-blog.appspot.com",
  messagingSenderId: "676279295492",
  appId: "1:676279295492:web:61c53fd9ace0d93a1c1f51"
};
  

var fire = firebase.initializeApp(config);
var db = firebase.firestore();
export default db;
export {fire};
  