import React, {useState} from 'react';
import { useHistory } from "react-router-dom";
import './Login.css';
import './Util.css';
import {fire} from '../flame/fire';

function Home() {
    let history = useHistory();

    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");

    function authenticate(){
        fire.auth().signInWithEmailAndPassword(email,password)
        .then(res => {
            console.log(res)
            history.push("/home");
        })
        .catch(function(error) {
            console.log(error.message);
        })
    }

  return (
    <div className="limiter">
        <div className="container-login100">
            <div className="wrap-login100 p-l-55 p-r-55 p-t-65 p-b-50">
                <div className="login100-form validate-form" method="post">
                    <span className="login100-form-title p-b-33">
                        Account Login
                    </span>

                    <div className="wrap-input100 validate-input" data-validate = "Valid email is required: ex@abc.xyz">
                        <input className="input100" type="text" name="email" placeholder="Email" value={email} onChange={e => setEmail(e.target.value)}/>
                        <span className="focus-input100-1"></span>
                        <span className="focus-input100-2"></span>
                    </div>

                    <div className="wrap-input100 rs1 validate-input" data-validate="Password is required">
                        <input className="input100" type="password" name="pass" placeholder="Password" value={password} onChange={e => setPassword(e.target.value)}/>
                        <span className="focus-input100-1"></span>
                        <span className="focus-input100-2"></span>
                    </div>

                    <div className="container-login100-form-btn m-t-20" onClick={authenticate}>
                        <button className="login100-form-btn">
                            Sign in
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
  );
}

export default Home;
