import React, { useState, useEffect } from 'react';

import { useHistory } from "react-router-dom";

import Header from "../Components/Header";
import Footer from "../Components/Footer";
import db from '../flame/fire';

import './Home.css';

function Home() {
    const [posts, setPosts] = useState([]);
    const [isLoading, setIsLoading] = useState(true);
    
    useEffect(() => {
        /*var user = fire.auth().currentUser;
        if (!user) {
            document.location.href="/";
        }*/
        console.log(Date.now());

        let blogPosts = [];
        let postRef = db.collection('posts').orderBy('TimeStamp','desc');
        postRef.get()
        .then(snapshot => {
            snapshot.forEach(doc => {
                let post = { "id": doc.id, "content" : doc.data()};
                blogPosts.push(post);
                //console.log(blogPosts);
                setPosts(blogPosts);
                //console.log(doc.id, '=>', doc.data());
            });
            setIsLoading(false);
        })
        .catch(err => {
            console.log('Error getting documents', err);
            setIsLoading(false);
        });
    }, []);

    let history = useHistory();

    function RedirectToFullPost(id) {
        history.push("/Post/"+id);
    }


  return (
      <div className="Home__Container">
          {isLoading  && <div className="Home__Loading__Screen">Loading...</div>}
          <Header/>
          <div className="Home__Head__Image">
            
            <div className="Home__Head__Image__Text">
                <div><h1>My internship @MassiveMedia</h1></div>
            </div>
          </div>
          <div className="Home__Wrapper">
          {posts.length === 0 ?
                (<div className="Blog__Posts">
                    <h2>Posts</h2>
                    <h3>Helaas zijn er nog geen posts geplaatst.</h3>
                </div>) :
                (<div className="Blog__Posts">
                    <h2>Posts</h2>
                    {posts.map((post, i) => {
                        return <div className="Blog__Post" key={i}>
                            <h3 className="Blog__Post__Title" onClick={() => RedirectToFullPost(post.id)}>{post.content.Title}</h3>
                            <p className="Blog__Post__Details">Geplaatst op {post.content.Date} door Quinten Verniers</p>
                        </div>
                    })}
                </div>)
            }
          </div>
          <Footer/>
      </div>
  );
}

export default Home;
