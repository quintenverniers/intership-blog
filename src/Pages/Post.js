import React, {useState, useEffect} from 'react';
import { useParams } from "react-router-dom";
import db from '../flame/fire';
import Header from "../Components/Header";
import Footer from "../Components/Footer";

import './Post.css';

function Home() {
    const [post, setPost] = useState({});
    const [isLoading, setIsLoading] = useState(true);
    let { id } = useParams();

    useEffect(() => {
        /*var user = fire.auth().currentUser;
        if (!user) {
            document.location.href="/";
        }*/

        let blogPost;
        var docRef = db.collection("posts").doc(id);
        docRef.get().then(function(doc) {
            blogPost = doc.data();
            if (doc.exists) {
                console.log("Document data:", doc.data());
                console.log(blogPost.Paragraphs);
                setPost(blogPost);
                setIsLoading(false);
            } else {
                // doc.data() will be undefined in this case
                console.log("No such document!");
                setIsLoading(false);
            }
        }).catch(function(error) {
            console.log("Error getting document:", error);
            setIsLoading(false);
        });
    }, [id]);
  return (
      <div className="Blog__Container">
           {isLoading  && <div className="Blog__Loading__Screen">Loading...</div>}
          <Header/>
          <div className="Blog__Head__Image" style={{backgroundImage: `url(${post.Image})`}}>
            <div className="Blog__Head__Image__Text">
                <div>
                    <h1 className="Blog__Title">{post.Title}</h1>
                    <p className="Blog__Details">Geplaatst op door Quinten Verniers</p>
                </div>
            </div>
          </div>
          <div className="Blog__Wrapper">
            <div className="Blog__Posts">
                {!isLoading && 
                    post.Paragraphs.map((para, i) => {
                        return <div className="Blog__Post" key={i}>
                            <h3 className="Blog__Post__Item__Title">{para.Title}</h3>
                            <p className="Blog__Post__Paragraph">{para.Text}</p>
                        </div>
                    })} 
            </div>
          </div>
          <Footer />
      </div>
  );
}

export default Home;
